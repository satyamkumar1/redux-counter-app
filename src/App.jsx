
import { useSelector} from 'react-redux'
import { useDispatch } from 'react-redux'
import { increment,decrement,reset,incrementby5 } from './Redux/store/Slice';
import "./App.css"

function App() {

  const count=useSelector(state=>state.counter.counter);
  const arr=useSelector(state=>state.counter.arr);

  const dispatch=useDispatch()
  console.log(arr);

  return (
    <>
    <div className='main'>
    <h1> Redux-Counter-App</h1>
    
      <h3>{count}</h3>
      <div className='counter-btn'>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <button onClick={() => dispatch(incrementby5())}>incrementby5</button>
      <button onClick={() => dispatch(reset())}>reset</button>
      </div>

      </div>
     
    </> 
  )
}

export default App
