import { configureStore } from "@reduxjs/toolkit";
import  user  from "./store/Slice";

const store=configureStore({
    reducer:{
        counter:user.reducer,
    }
})
export  default store;  