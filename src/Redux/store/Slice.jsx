import { createSlice } from "@reduxjs/toolkit";

const initialState={
    counter:0    
}
const user= createSlice({

    name:"count",
    initialState:initialState,
    reducers:{

        increment(state){
            
            state.counter++;
        },
        decrement(state){
            state.counter--;       

        },
        reset(state){
            state.counter=0;
           
        },
        incrementby5(state){
            state.counter+=5;
        }
        


    }
})

export const { increment, decrement ,reset,incrementby5} =user.actions;
export default user;